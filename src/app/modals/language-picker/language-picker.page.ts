import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Language } from '../../models/language';
import { LanguagesService } from '../../services/languages.service';

@Component({
  selector: 'app-language-picker',
  templateUrl: './language-picker.page.html',
  styleUrls: ['./language-picker.page.scss'],
})
export class LanguagePickerPage {
  @Input() selected: string;
  @Input() includeAuto: boolean;
  @Input() title: string;
  private languages: Language[] = [];
   
  constructor(private modalCtrl: ModalController, private languagesService: LanguagesService) { }

   ngOnInit() {
    this.languagesService.getCached().subscribe(languages => {      
      this.languages = languages;
      if (!this.includeAuto) {
        this.languages = this.languages.filter(l => l.code !== 'auto');
      }
    });
  }
  
  public select(lang: string) {
    this.selected = lang;
    this.modalCtrl.dismiss(this.selected);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { CropperComponent } from 'angular-cropperjs';
import { ModalController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-picture-picker',
  templateUrl: './picture-picker.page.html',
  styleUrls: ['./picture-picker.page.scss'],
})
export class PicturePickerPage implements OnInit {
  @ViewChild('angularCropper') public angularCropper: CropperComponent;

  public croppedImage = null;
  public image = null;
  public imageUrl;

  scaleValX = 1;
  scaleValY = 1;
  cropperOptions = {
    viewMode: 2,
    dragMode: 'move',
    zoomOnTouch: true,
    autoCrop: true,
    movable: true,
    zoomable: true,
    scalable: true,
    autoCropArea: 0.3,
    minContainerHeight: window.screen.height * 0.83,
    minCanvasHeight: window.screen.height * 0.83
  };
 
  constructor(
    private modalCtrl: ModalController,
    public platform: Platform,
    ) { }

  ngOnInit() {
    if (!this.platform.is('android')) {
      this.cropperOptions.minCanvasHeight = window.screen.height * 0.4;
      this.cropperOptions.minContainerHeight = window.screen.height * 0.4;
      return;
    }
  }
  
  get imageSrc() {
    return this.imageUrl ? this.imageUrl : this.image;
  }
  reset() {
    this.angularCropper.cropper.reset();
  }

  rotate() {
    this.angularCropper.cropper.rotate(90);
  }
 
  save() {
    const croppedImgB64String: string = this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg', (100 / 100));
    this.croppedImage = croppedImgB64String;

    this.modalCtrl.dismiss({croppedImage: this.croppedImage});
  }

  cancel() {
    this.modalCtrl.dismiss({croppedImage: null});
  }
}

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';

import { HomePage } from './home.page';
import { LanguagePickerPageModule } from '../modals/language-picker/language-picker.module';
import { ComponentsModule } from '../components/components.module';
import { PicturePickerPageModule } from '../modals/picture-picker/picture-picker.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LanguagePickerPageModule,
    PicturePickerPageModule,
    RouterModule.forChild([{ path: '', component: HomePage }]),
    ComponentsModule
  ],
  providers: [Camera],
  declarations: [HomePage]
})
export class HomePageModule {}

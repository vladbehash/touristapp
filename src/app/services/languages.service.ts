import { Injectable } from '@angular/core';
import { Observable, from, combineLatest } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { Language } from '../models/language';
import { StorageKeyConstants } from '../constants/storageKeys';
@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  constructor(private http: HttpClient,
    private storage: Storage) { }

  public get(): Observable<Language[]> {
    return this.http
      .get('assets/data/languages.json')
      .pipe(map((languages: Language[]) => {
        return languages.map((l) => l as Language);
      }));
  }

  public getCached(): Observable<Language[]> {
    const fromStorage = from(this.storage.get(StorageKeyConstants.languages))
      .pipe(map(languages => languages as Language[]));

    const fromSource = this.get()
      .pipe(map(languages => {
        from(this.storage.set(StorageKeyConstants.languages, languages)).subscribe();
        return languages;
      }));

    return combineLatest(fromStorage, fromSource, (storageLanguages, sourceLanguages) => {
      if (storageLanguages) {
        console.log('from storage');
        return storageLanguages;
      }
      console.log('from source');
      return sourceLanguages;
    });
  }

  public getCachedAsMap(): Observable<Map<string, string>> {
    return this.getCached()
    .pipe(map(languages => this.asMap(languages)));
  }

  private asMap(languages: Language[]): Map<string, string> {
    return new Map<string, string>(languages.map(l => [l.code, l.name] as [string, string]));
  }
}

import { Injectable } from '@angular/core';
import { LanguageSetup } from '../models/language-setup';
import { from, Observable } from 'rxjs';
import { HTTP as Http } from '@ionic-native/http/ngx';
import { ConfigConstants } from '../constants/config';
import { map } from 'rxjs/operators';
import * as querystring from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor(private http: Http) { }

  public getTranslation(text: string, setup: LanguageSetup): Observable<any> {
    const params = {
      q: text,
      source: setup.from,
      target: setup.to,
      key: ConfigConstants.GoogleApiKey
    };
    const queryParams = querystring.stringify(params);
    console.log(this.http.getHeaders(ConfigConstants.GoogleApi));

    return from(this.http.post(`${ConfigConstants.GoogleApi}?${queryParams}`, {}, {}))
      .pipe(map((res: any) => this.mapResult(JSON.parse(res.data).data, text, setup)));
  }

  private mapResult(data, text: string, setup: LanguageSetup) {
    const result = {
      text: text,
      translatedText: data.translations[0].translatedText,
      setup: Object.assign({}, setup)
    };

    return result;
  }
}

import { TestBed } from '@angular/core/testing';

import { RouteMapNavigationService } from './route-map-navigation.service';

describe('RouteMapNavigationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RouteMapNavigationService = TestBed.get(RouteMapNavigationService);
    expect(service).toBeTruthy();
  });
});

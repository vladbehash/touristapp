export enum ErrorsConstants {
    TranslateServiceError = 'Error occured while translating...',
    OcrServiceError = 'Error occured while recognizing...',
    GeolocationError = 'Error occured while getting your location...',
    GoogleMapError = 'Error occured while initializing map...'
}

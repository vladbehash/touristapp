export enum StorageKeyConstants {
    languages = 'languages',
    translations = 'translations',
    settings = 'settings'
}

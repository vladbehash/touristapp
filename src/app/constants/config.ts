import { DestinationType, EncodingType, MediaType, PictureSourceType } from '@ionic-native/camera/ngx';

export class ConfigConstants {
    static GoogleApi = 'https://translation.googleapis.com/language/translate/v2';
    static GoogleApiKey = 'AIzaSyAUqnlPjatkVRY135xorj-TzXdQxC2y5hA';

    static DefaultLocation = {lat: 49.9935, lng: 36.2304};
    static CameraOptions = {
        quality: 50,
        destinationType: DestinationType.DATA_URL,
        encodingType: EncodingType.JPEG,
        mediaType: MediaType.PICTURE,
        sourceType: PictureSourceType.CAMERA,
        correctOrientation: true
      };
    static GeolocationOptions = {
        timeout: 4000,
        enableHighAccuracy: true
    };

    static DefaultFromLang = 'en';
    static DefaultToLang = 'uk';
}

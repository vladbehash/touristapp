import { ToastOptions } from '@ionic/core';

export class ToastOptionsFactory {
    static error(msg): ToastOptions {
        return {
            message: msg,
            showCloseButton: true,
            position: 'top',
            color: 'danger',
            duration: 2000
        };
    }

    static warning(msg): ToastOptions {
        return {
            message: msg,
            showCloseButton: true,
            position: 'top',
            color: 'warning',
            duration: 2000
        };
    }

    static success(msg): ToastOptions  {
        return {
            message: msg,
            showCloseButton: true,
            position: 'top',
            color: 'success',
            duration: 2000
        };
    }
}

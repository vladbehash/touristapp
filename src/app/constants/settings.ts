export enum SettingsConstants {
    GeolocationEnabledSuccess = 'Geolocation enabled!',
    GeolocationDisabledSuccess = 'Geolocation disabled!',
    MemosClearSuccess = 'Memos cleared successfully!',
}

import { Component, OnInit } from '@angular/core';
import { ConfigConstants } from '../constants/config';
import { Platform, ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Coords } from '../models/coords';
import { ErrorsConstants } from '../constants/errors';
import { ToastOptionsFactory } from '../constants/toast-options-factory';
import { MemosService } from '../services/memos.service';
import { SettingsService } from '../services/settings.service';
import { SettingsConstants } from '../constants/settings';

declare var google;

@Component({
  selector: 'app-contact',
  templateUrl: 'contact.page.html',
  styleUrls: ['contact.page.scss']
})
export class ContactPage implements OnInit {

  public isAnyMemos = true;

  constructor(
    private settingsService: SettingsService,
    private memosService: MemosService,
    private toastController: ToastController) {
  }

  async ngOnInit() {
    this.memosService.trackingSubject().subscribe(async () => {
      this.isAnyMemos = await this.memosService.any();
    });
  }

  async clearMemos() {
    await this.memosService.clear();
    const toast = await this.toastController.create(
      ToastOptionsFactory.success(SettingsConstants.MemosClearSuccess));
    await toast.present();
  }

  get isMemosClearEnabled(): boolean {
    return this.isAnyMemos;
  }
  get isSavingGeolocationEnabled(): boolean {
    return this.settingsService.saveGeolocation;
  }

  async savingGeolocationClicked(checked: boolean) {
    this.settingsService.saveGeolocation = checked;
    console.log(event);
    let message: string = null;
    if (checked) {
      message = SettingsConstants.GeolocationEnabledSuccess;
    } else {
      message = SettingsConstants.GeolocationDisabledSuccess;
    }
    const toast = await this.toastController.create(
      ToastOptionsFactory.success(message));
    await toast.present();
  }
}

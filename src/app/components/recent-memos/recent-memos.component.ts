import { Component, OnInit, Input } from '@angular/core';
import { RecentMemos } from '../../models/view/recent-memos';
import { Translation } from '../../models/translation';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recent-memos',
  templateUrl: './recent-memos.component.html',
  styleUrls: ['./recent-memos.component.scss']
})
export class RecentMemosComponent implements OnInit {

  @Input() recentMemos: RecentMemos = null;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  get recents(): Translation[] {
    return this.recentMemos.recents;
  }

  get last(): Translation {
    return this.recentMemos.last;
  }
}

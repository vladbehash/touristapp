export class LanguageSetup {
    constructor(
        public from: string,
        public to: string) {
    }
}
